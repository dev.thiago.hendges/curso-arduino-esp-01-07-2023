#include <WiFi.h>
#include <ESPAsyncWebSrv.h>

const char* ssid = "ThiagoRocha";     // Nome da rede WiFi do Access Point
const char* password = "123456789"; // Senha do Access Point

int ledPin = 2; // Pino do LED da placa ESP32
String html = "";

AsyncWebServer server(80);

void setup() {
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);

  Serial.begin(115200);
  WiFi.softAP(ssid, password);

  html += "<html><body>";
  html += "<h1>Controle do LED via WiFi</h1>";
  html += "<p><a href=\"/on\"><button>Ligar LED</button></a></p>";
  html += "<p><a href=\"/off\"><button>Desligar LED</button></a></p>";
  html += "</body></html>";

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "text/html", html);
  });

  server.on("/on", HTTP_GET, [](AsyncWebServerRequest *request){
    digitalWrite(ledPin, HIGH);
    request->send(200, "text/html", html);
  });

  server.on("/off", HTTP_GET, [](AsyncWebServerRequest *request){
    digitalWrite(ledPin, LOW);
    request->send(200, "text/html", html);
  });

  server.begin();
}

void loop() {
}
