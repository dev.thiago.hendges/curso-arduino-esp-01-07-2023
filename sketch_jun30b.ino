void setup() {
  pinMode(A0, INPUT);
  pinMode(13, OUTPUT);
}

void loop() {
  int valorSensor = analogRead(A0);

  if(valorSensor > 512){
    digitalWrite(13, HIGH);
  }else{
    digitalWrite(13, LOW);
  }
}
